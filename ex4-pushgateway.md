# PushGateway

## Présentation

Pushgateway est autre façon de présenter des métriques à prometheus.<br>
cet outil propose une interface pour prometheus pour la récupération des métriques mais aussi une API via HTTP.<br>
Il conserve la dernière valeur des métriques envoyés par requête HTTP pour les proposer au format prometheus.<br>
Cela ouvre la porte à la propagation de métriques via l'utilisation de langage n'ayant pas d'API prometheus, pour exemple script, ansible, ....<br>
Nous allons dans les exercices mettre en oeuvre la propagation de métrique à l'aide d'un simple script bash.

## Construction d'un dashboard

http://localhost/pushgateway

**exercice :** Création d'un script pour la collecte d'information disque et propagation des résultats vers pushgateway

<details><summary>Solution</summary>
<p>
création d'un script de scan de dossier et récupération des tailles de fichier<br>
<pre><code>#!/bin/bash

currentFolder=$(basename $PWD)
folderSize=$(du -sk $PWD | awk '{print  $1}')
echo "----------------------------"
echo "$currentFolder - $folderSize"

echo "----------------------------"
for file in *
do
    size=$(du -sk $file | awk '{print  $1}')
    echo "$file - $size ko"
    body="$body\nfile_size{file=\"$file\"} $size"
done

echo "----------------------------"
echo "push metrics"
cat <<EOF | curl -X PUT --data-binary @- http://localhost/pushgateway/metrics/job/script/folder/$currentFolder
# TYPE file_size gauge
$(echo -e ${body#\\n})
# TYPE folder_size gauge
# HELP folder_size Root folder size.
folder_size $folderSize
EOF</code></pre><br>
Vérification de l'enregistrements des métriques<br>
<img src="img/ex4-1a.png"><br>
</p>
</details>

**exercice :** Construction d'un dashboard exploitant les données de pushgateway

<details><summary>Solution</summary>
<p>
Création d'un Panel de type <b>Pie chart</b><br>
<pre><code>sum by (file) (file_size)</code></pre><br>
<img src="img/ex4-2a.png"><br>
Modification des propriétés du graphique</b><br>
<img src="img/ex4-2b.png"><br>
</p>
</details>

[< Previous](ex3-grafana.md) | [Home](README.md) | [Next >](ex5-loki.md)