#!/bin/bash

set -o errexit
set -o pipefail

export HOST=http://localhost/grafana
export KEY=xxxxxxxxxxxxxxxxxxxx

[ -n "$KEY" ] || ( echo "No API key found. Get one from $HOST/org/apikeys and run 'KEY=<API key> $0'"; exit 1)

set -o nounset

echo "Exporting Grafana dashboards from $HOST"
rm -rf temp
mkdir -p temp
for dash in $(curl -s -H "Authorization: Bearer $KEY" "$HOST/api/search?query=&" | jq -r '.[] | select(.type == "dash-db") | .uid'); do
  echo "-----------------------------"
	echo "extract dashboard ${dash} - BEGIN"
	curl -s -H "Authorization: Bearer $KEY" "$HOST/api/dashboards/uid/$dash" | jq -r > temp/${dash}.json
	slug=$(cat temp/${dash}.json | jq -r '.meta.slug')
	mv temp/${dash}.json temp/${slug}.json
  echo "extract dashboard ${dash}/${slug} - DONE"
done
