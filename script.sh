#!/bin/bash

currentFolder=$(basename $PWD)
folderSize=$(du -sk $PWD | awk '{print  $1}')
echo "----------------------------"
echo "$currentFolder - $folderSize"

echo "----------------------------"
for file in *
do
    size=$(du -sk $file | awk '{print  $1}')
    echo "$file - $size ko"
    body="$body\nfile_size{file=\"$file\"} $size"
done

echo "----------------------------"
echo "push metrics"
cat <<EOF | curl -X PUT --data-binary @- http://localhost/pushgateway/metrics/job/script/folder/$currentFolder
# TYPE file_size gauge
$(echo -e ${body#\\n})
# TYPE folder_size gauge
# HELP folder_size Root folder size.
folder_size $folderSize
EOF