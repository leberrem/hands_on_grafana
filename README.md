# GRAFANA

Presentation et prise en main de grafana au-travers d'une stack de supervision locale

## Documentation :
* [Grafana](https://grafana.com/docs/)
* [Prometheus](https://prometheus.io/docs/prometheus/latest/getting_started/)

## L'infrastructure cible

![infra](./img/infra.drawio.png)

## Le dashboard cible

![dashboard](./img/dashboard.png)

## Hands-On
[Commencer le hands-on](ex0-getting-started.md)

Accès direct aux exercices :
* [Exo0 - Les prérequis](ex0-getting-started.md)
* [Exo1 - Les exporter](ex1-exporter.md)
* [Exo2 - Prometheus](ex2-prometheus.md)
* [Exo3 - Grafana](ex3-grafana.md)
* [Exo4 - Pushgateway](ex4-pushgateway.md)
* [Exo5 - Loki](ex5-loki.md)
* [Exo6 - API](ex6-api.md)
