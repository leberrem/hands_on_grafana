# Grafana

## Présentation

Grafana est principalement un outil permettant de mettre en oeuvre des dashboard de supervision.<br>
Il est compatible avec une multitude de sources de données.<br>
Dans notre cas nous mettrons prometheus en base de données par défaut.<br>
Pour chaque base de données, il est necessaire d'utiliser le langage de requetage adapté.<br>
Pour prometheus nous utiliserons la syntaxe promQL.

## Construction d'un dashboard

http://localhost/grafana
> user: admin<br>
> password: admin

**exercice :** Création d'un dashboard

<details><summary>Solution</summary>
<p>
création du dashboard<br>
<img src="img/ex3-1a.png"><br>
création d'un panel<br>
<img src="img/ex3-1b.png"><br>
Définition de la requête<br>
<pre><code>(sum(irate(node_cpu_seconds_total{mode='user'}[5m])) / count(node_cpu_seconds_total{mode='user'})) * 100</code></pre>
<img src="img/ex3-1c.png"><br>
Modification de l'unité<br>
<img src="img/ex3-1d.png"><br>
Modification du titre<br>
<img src="img/ex3-1e.png"><br>
Présentaiton du résultat<br>
<img src="img/ex3-1f.png"><br>
</p>
</details>

**exercice :** Mise en oeuvre d'un graphique sur la consommation CPU des conteneurs (multi séries)

<details><summary>Solution</summary>
<p>
Duplication du premier panel<br>
<img src="img/ex3-2a.png"><br>
Définition de la requête et légende<br>
<pre><code>rate(container_cpu_user_seconds_total{image!=""}[5m]) * 100</code></pre><br>
<img src="img/ex3-2b.png"><br>
Modification des propriétés du graphique<br>
<img src="img/ex3-2c.png"><br>
</p>
</details>

**exercice :** Mise en oeuvre d'un graphique sur l'espace disque (les statistiques)

<details><summary>Solution</summary>
<p>
construction d'un panel <b>Stat</b><br>
<img src="img/ex3-3a.png"><br>
Définition de la requête et légende<br>
<pre><code>node_filesystem_size_bytes</code></pre><br>
<img src="img/ex3-3b.png"><br>
Modification des propriétés du panel<br>
<img src="img/ex3-3c.png"><br>
</p>
</details>

**exercice :** Mise en oeuvre d'un graphique sur l'espace disque disponible (les seuils)

<details><summary>Solution</summary>
<p>
construction d'un panel <b>Stat</b> et définition de la requête et légende<br>
<pre><code>(node_filesystem_size_bytes-node_filesystem_avail_bytes)*100/node_filesystem_size_bytes</code></pre><br>
<img src="img/ex3-4a.png"><br>
Définition des seuils<br>
<img src="img/ex3-4b.png"><br>
<img src="img/ex3-4c.png"><br>
Modification des propriétés du graphique<br>
<img src="img/ex3-4d.png"><br>
</p>
</details>

**exercice :** Mise en oeuvre des variables et lignes dynamiques de rupture sur les statistiques des conteneurs

<details><summary>Solution</summary>
<p>
Création d'une variable pour lister les conteneurs<br>
<img src="img/ex3-5a.png"><br>
Création d'une rupture dynamique<br>
<img src="img/ex3-5b.png"><br>
Ajout de plusieurs panel dans la zone de rupture dynamique<br>
<pre><code>container_memory_usage_bytes{image!="", name=~"$container"}</code></pre><br>
<pre><code>(sum(irate(container_cpu_user_seconds_total{name=~"$container"}[5m])) / count(container_cpu_user_seconds_total{name=~"$container"})) * 100</code></pre><br>
<img src="img/ex3-5c.png"><br>
</p>
</details>

**exercice :** Mise en oeuvre d'un tableau et enrichissements graphique sur les statistiques réseaux des conteneurs

<details><summary>Solution</summary>
<p>
Création d'unpanel de type <b>Table</b><br>
<pre><code>sum by (name) (irate(container_network_receive_bytes_total{image!=""}[5m]))</code></pre><br>
<pre><code>sum by (name) (irate(container_network_transmit_bytes_total{image!=""}[5m]))</code></pre><br>
<img src="img/ex3-6a.png"><br>
Ajout d'une jointure<br>
<img src="img/ex3-6b.png"><br>
Ajout d'une transformation pour réorganiser et renommer les champs<br>
<img src="img/ex3-6c.png"><br>
Ajout de surcharge pour l'affichage des zones<br>
<img src="img/ex3-6d.png"><br>
Finalisation de l'affichage<br>
<img src="img/ex3-6e.png"><br>

</p>
</details>

[< Previous](ex2-prometheus.md) | [Home](README.md) | [Next >](ex4-pushgateway.md)