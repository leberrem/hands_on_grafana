# Les exporter

## Présentation

Les exporters sont des agents qui proposent une interface HTTP type OPENTELEMETRY mettant à disposition un état des services surveillés au moment de leur consultation.

Liste des URLs des metriques :

* http://localhost:8082/metrics
* http://localhost/cadvisor/metrics
* http://localhost/node/metrics
* http://localhost/prometheus/metrics
* http://localhost/pushgateway/metrics

> le métriques sont exposées par un agent ou directement par le service. Par convention, le point d'entrée est sous le path "/metrics"

![node_exporter](./node_exporter.png)

## Analyse d'un metrique

```sh
# HELP node_filesystem_avail_bytes Filesystem space available to non-root users in bytes.
# TYPE node_filesystem_avail_bytes gauge
node_filesystem_avail_bytes{device="rootfs",fstype="tmpfs",mountpoint="/"} 3.116138496e+09
```

* **HELP** : Zone de description du métrique
* **TYPE** : Type de métrique exposé
* [nom du metrique]**{**[label clef/valeur]**}** [valeur numérique]

## Les types de métrique

* **gauge** : Jauge - Valeur numérique unique pouvant être définie de manière arbitraire.
* **counter** : Compteur - Valeur unique qui ne peut être réinitialisée sur 0 au redémarrage ou augmentée de façon monotone.
* **histogram** : Histogramme - Groupe de buckets configurables permettant d'effectuer un échantillonnage et d'enregistrer des valeurs dans des plages, et fournit également la somme de toutes les valeurs observées.
* **summary** : Sommaire - Semblable à un histogramme, il calcule également des quantiles configurables sur une fenêtre à durée flexible.

## coder un exporter

La fonctionnalité d'exporter est très simple à mettre en oeuvre car prometheus fournit des API et librairies pour différents langages et une grande quatité d'exemples d'implémentation.

* https://prometheus.io/docs/instrumenting/writing_exporters/
* https://prometheus.io/docs/instrumenting/exporters/

Exemples de projet MGDIS :

* https://gitlab.mgdis.fr/ops/outils/sslscore-exporter (PYTHON)
* https://gitlab.mgdis.fr/ops/outils/ovh-exporter (PYTHON)
* https://gitlab.mgdis.fr/ops/infrastructure/pda-exporter (GO)
* https://gitlab.mgdis.fr/ops/infrastructure/redis-exporter (GO)

[< Previous](ex0-getting-started.md) | [Home](README.md) | [Next >](ex2-prometheus.md)