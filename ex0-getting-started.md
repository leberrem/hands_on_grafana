# Les prérequis

Les différentes exercices vont necessiter l'utilisation de docket et docker-compose.

## MacOS
Installer [docker](https://docs.docker.com/desktop/mac/install/)

> De préférence mettre `4Go` pour docker for mac

## Linux (ubuntu)
Installer [docker](https://docs.docker.com/engine/install/ubuntu/)<br>
Installer [docker-compose]
```sh
sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
```

# Démarrage

Lancement de la stack docker

```sh
$ docker-compose pull
$ docker-compose up -d
```

# Les services démarrés

* **node_exporter** : agent de suivi des métriques du système hote
* **cadvisor** : agent de suivi des métriques docker
* **traefik** : reverse proxy
* **grafana** : interface de supervision
* **prometheus** : Base de données des métriques
* **alertmanager** : hub de notification

# URL des interfaces

* traefik : http://localhost:8080/dashboard/
* grafana : http://localhost/grafana/
* prometheus : http://localhost/prometheus/
* alertmanager : http://localhost/alertmanager/
* pushgateway : http://localhost/pushgateway/

> Compte grafana par défaut : admin/admin

# Arrêt et purge des volumes

```sh
$ docker-compose down
$ docker volume prune
```

[Home](README.md) | [Next >](ex1-exporter.md)

