# Loki

## Présentation

Loki est un outil d'indexation et centralisation de traces techniuqes et applicative.<br>
L'alimentation de la base loki passe par la mise en oeuvre d'agents locaux qui s'occupe d'effectuer la collecte et la transmission sous forme de flux vers loki en http.<br>
A l'égal de prometheus la consultation des données s'effectue au-travers d'un langage de requêtage dédié appelé LogQL.

## Construction d'un dashboard

http://localhost/grafana

**exercice :** Consultation des traces du conteneur **grafana**

<details><summary>Solution</summary>
<p>
Ouverture de l'explorer grafana pour le requêtage libre.<br>
<img src="img/ex5-1a.png"><br>
Affichage du log browser et selection des zones.<br>
<img src="img/ex5-1b.png"><br>
Requête de consultation.<br>
<pre><code>{container_name="grafana"}</code></pre><br>
<img src="img/ex5-1c.png"><br>
Suivi en temps réel des traces.<br>
<img src="img/ex5-1d.png"><br>
</p>
</details>

**exercice :** Création d'un live log sur les traces du conteneur **grafana**

<details><summary>Solution</summary>
<p>
Création d'un panel de type <b>logs</b>.<br>
<img src="img/ex5-2a.png"><br>
Modification de la source de données et requête logQL.<br>
<pre><code>{container_name="grafana"}</code></pre><br>
<img src="img/ex5-2b.png"><br>
Mise en forme et visualisation.<br>
<img src="img/ex5-2c.png"><br>
</p>
</details>

**exercice :** Graphique de compteur des traces envoyées par seconde pour chaque conteneur

<details><summary>Solution</summary>
<p>
Création d'un panel de type <b>time series</b>.<br>
<img src="img/ex5-3a.png"><br>
Modification de la source de données et requête logQL.<br>
<pre><code>rate({service="docker"}[2m])</code></pre><br>
<img src="img/ex5-3b.png"><br>
Mise en forme et visualisation.<br>
<img src="img/ex5-3c.png"><br>
</p>
</details>

**exercice :** Graphique de compteur des erreurs par **conteneur**

<details><summary>Solution</summary>
<p>
Création d'un panel de type <b>Pie Chart</b>.<br>
<img src="img/ex5-4a.png"><br>
Modification de la source de données et requête logQL.<br>
<pre><code>count_over_time({service="docker"} |= "error" [1m])</code></pre><br>
<img src="img/ex5-4b.png"><br>
Mise en forme et visualisation.<br>
<img src="img/ex5-4c.png"><br>
</p>
</details>

**exercice :** Graphique de compteur des traces par **level**

<details><summary>Solution</summary>
<p>
Création d'un panel de type <b>time series</b>.<br>
<img src="img/ex5-5a.png"><br>
Modification de la source de données et requête logQL.<br>
<pre><code>sum  by (level) (count_over_time({service="docker"} | regexp `.*level=(?P<level>.*? ).*` | regexp `.*lvl=(?P<level>.*? ).*` | level !="" [1m]))</code></pre><br>
<img src="img/ex5-5b.png"><br>
Mise en forme et visualisation.<br>
<img src="img/ex5-5c.png"><br>
</p>
</details>

[< Previous](ex4-pushgateway.md) | [Home](README.md) | [Next >](ex6-api.md)