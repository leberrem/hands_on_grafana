# API

## Présentation

Grafana propose une API permettant d'exploiter le produit ou d'intégrer celui-ci dans des outils d'exploitation indépendants.<br>
Nous allons mettre en oeuvre son usage au travers des fonctions d'importation et d'exportation de dashboard.

https://grafana.com/docs/grafana/latest/http_api/

## Import/Export de dashboard

http://localhost/grafana

**exercice :** Création d'une clef d'API d'importation

<details><summary>Solution</summary>
<p>
http://localhost/grafana/org/apikeys.<br>
<img src="img/ex6-1a.png"><br>
création d'une clef d'importation avec les droits <b>Editor</b>.<br>
<img src="img/ex6-1b.png"><br>
Test d'appel.<br>
<pre><code>curl -H "Authorization: Bearer xxxxxxxxxxxxxxxxx" http://localhost/grafana/api/dashboards/home | jq</code></pre><br>
</p>
</details>

**exercice :** Importation du dashboard des solutions

<details><summary>Solution</summary>
<p>
Commande d'importation.<br>
<pre><code>curl -v -X POST -s -H "Authorization: Bearer xxxxxxxxxxxxxxxxx" -H "Content-Type: application/json" -d @soluces.json "http://localhost/grafana/api/dashboards/db/"</code></pre><br>
Visualisation du dashboard.<br>
<img src="img/ex6-2a.png"><br>
<img src="img/ex6-2b.png"><br>
</p>
</details>

**exercice :** Exportation en masse de dashboard

<details><summary>Solution</summary>
<p>
Création d'une clef d'exportation avec les droits <b>viewer</b>.<br>
<img src="img/ex6-3a.png"><br>
Création d'un script d'export. (export.sh)<br>
<blockquote>Necessite la présence de l'executable <b>jq</b> pour parcourir le flux json</blockquote><br>
<img src="img/ex6-3b.png"><br>
Execution du script.<br>
<pre><code>Exporting Grafana dashboards from http://localhost/grafana
-----------------------------
extract dashboard YiPmJDtWk - BEGIN
extract dashboard YiPmJDtWk/monitor-prometheus - DONE
-----------------------------
extract dashboard EtF61vtZz - BEGIN
extract dashboard EtF61vtZz/monitoring-containers - DONE
-----------------------------
extract dashboard EWEmJvpZz - BEGIN
extract dashboard EWEmJvpZz/monitoring-host - DONE
-----------------------------
extract dashboard qPdAviJmz - BEGIN
extract dashboard qPdAviJmz/monitoring-traefik - DONE
-----------------------------
extract dashboard jWVIGe_nz - BEGIN
extract dashboard jWVIGe_nz/soluces - DONE</code></pre><br>
</p>
</details>

[< Previous](ex5-loki.md) | [Home](README.md) | [Next >](ex7-xxxxxx.md)