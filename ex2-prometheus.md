# Prometheus

## Présentation

Prometheus est principalement une base de données temporelle permettant d'enregistrer et d'aggréger les métriques.
En plus de la BDD, prometheus propose :
* Une interface de collecte basée sur une cartographie statique ou dynamique des agents (exporter) à collecter.
* Un système de requêtage pour la consultation des données de métrique au travers du langage **PromQL**.
* Un système de surveillance automatique basé sur des seuils d'alerte décrits en promQL.

## les interfaces de collecte

* http://localhost/prometheus/targets

Les points de collecte peuvent être définis par fichier de configuration.

```yaml
scrape_configs:
  - job_name: 'cadvisor'
    scrape_interval: 10s
    static_configs:
      - targets: ['cadvisor:8080']
```

Il est possible d'avoir des listes dynamique via des références à des fichiers externes ou des filtres sur des service d'inventaire type consul

```yaml
scrape_configs:
  - job_name: consul_class_astreinte
    scrape_interval: 1m
    consul_sd_configs:
      - server: '172.26.10.20:8500'
        datacenter: 'ovh'
    tls_config:
      insecure_skip_verify: true
    relabel_configs:
      - source_labels: [__meta_consul_tags]
        regex: '.*,metrics,.*'
        action: keep
      - source_labels: [__meta_consul_tags]
        regex: '.*,class:astreinte,.*'
        action: keep
```

> Il est necessaire de bien adapter la valeur de scrape_interval afin de ne pas saturer le système de collecte de prometheus dans le cas d'une grande quantité de point de collecte mais aussi de faire grossir la BDD avec des métriques inutiles.

## l'interface de consultation promQL

* http://localhost/prometheus/graph

**exercice :** Afficher la mémoire disponible.

<details><summary>Solution</summary>
<p>
<pre><code>node_memory_MemFree_bytes</code></pre>
<br>
<img src="img/ex2-1.png">
</p>
</details>

**exercice :** Afficher l'espace disque disponible pour pour le point de montage "/"

<details><summary>Solution</summary>
<p>
<pre><code>node_filesystem_free_bytes{mountpoint="/"}</code></pre>
<br>
<img src="img/ex2-2.png">
</p>
</details>

**exercice :** Afficher l'espace disque disponible pour pour le point de montage "/" en pourcentage

<details><summary>Solution</summary>
<p>
<pre><code>node_filesystem_free_bytes{mountpoint="/"}*100/node_filesystem_size_bytes{mountpoint="/"}</code></pre>
<br>
<img src="img/ex2-3.png">
</p>
</details>

**exercice :** Afficher la variation en pourcentage par seconde de consommation user CPU du conteneur prometheus par extrapolation de 5m

<details><summary>Solution</summary>
<p>
<pre><code>rate(container_cpu_user_seconds_total{name="prometheus"}[5m]) * 100</code></pre>
<br>
<img src="img/ex2-4.png">
</p>
</details>

## les règles d'alerting

* http://localhost/prometheus/alerts
* http://localhost/prometheus/rules

**exercice :** Créer une alerte sur le user CPU supérieur à 80%

<details><summary>Solution</summary>
<p>
Modification du fichier <a href="resources/prometheus/alert.rules">alert.rules<a><br>
<pre><code>
  - alert: high_cpu_user
    expr: (sum(irate(node_cpu_seconds_total{mode='user'}[5m])) / count(node_cpu_seconds_total{mode='user'})) * 100 > 80
    for: 30s
    labels:
      severity: warning
    annotations:
      summary: "Server under high CPU user"
      description: ""
</code></pre><br>
rechargement de la configuration de prometheus<br>
<pre><code>curl -X POST http://localhost/prometheus/-/reload</code></pre><br>
<img src="img/ex2-5.png">
</p>
</details>

**exercice :** Déclencher l'alerte avec un test de stress

<details><summary>Solution</summary>
<p>
Lancement d'un stress test<br>
<pre><code>docker run --rm -it progrium/stress --cpu 8 --timeout 60s</code></pre><br>
Le statut passe en PENDING avant le délais de "for:"<br>
<img src="img/ex2-6a.png"><br>
Le statut passe en FIRING passé le délais de "for:"<br>
<img src="img/ex2-6b.png"><br>
Vérification sur le graphique<br>
<img src="img/ex2-6c.png"><br>
</p>
</details>

**exercice :** Vérifier de la propagation de l'alerte dans alertmanager

<details><summary>Solution</summary>
<p>
Lancement d'un stress test<br>
<pre><code>docker run --rm -it progrium/stress --cpu 8 --timeout 60s</code></pre><br>
Lors du passage en firing on peut voir l'alerte en cours dans alertmanager<br>
<a href="http://localhost/alertmanager/#/alerts">http://localhost/alertmanager/#/alerts</a><br>
<img src="img/ex2-7.png"><br>
</p>
</details>

**exercice :** Configuration des canaux de notification

<details><summary>Solution</summary>
<p>
Modification du fichier de configuration <a href="resources/alertmanager/config.yml">config.yml<a><br>
<a href="https://prometheus.io/docs/alerting/latest/configuration/">https://prometheus.io/docs/alerting/latest/configuration/</a><br>
</p>
</details>


[< Previous](ex1-exporter.md) | [Home](README.md) | [Next >](ex3-grafana.md)